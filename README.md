Esse projeto foi desenvolvido utilizando ReactJS e está rodando por padrão na porta 3000 - então libere a porta ou mude a configuração para utilizar outra porta.

Esse projeto está consumindo a API disponível em https://gitlab.com/senac-projetos-de-desenvolvimento/2022-yuri-nobre-roza/playbook_api_ts

Para rodar o projeto faça o download do repositório abra o terminal (via VSCode ou pelo cmd) e siga os passos abaixo:

1) npm i - para instalar as dependencias do projeto
2) npm start - para carregar o projeto

Para testar melhor recomendo o download da API citada acima.
Com a API rodando é possível se cadastrar no site para liberar as outras funções - sem estar logado vai conseguir acessar apenas a página inicial, página de login e página de cadastro.
