import React, { useContext, useEffect, useState } from 'react';
import { useForm  } from 'react-hook-form';
import Conecta from './Conecta';
import { UserContext } from './UserContext';

const CadLivro = () => {
    const { register, handleSubmit, setValue } = useForm()

    const user = useContext(UserContext)

    const [selectValue, setSelectValue] = useState()

    const [autores, setAutores] = useState([])
    const getAutores = async () => {
        const listaAutores = await Conecta.get('/autor')

        setAutores(listaAutores.data)
    }

    const [editoras, setEditoras] = useState([])
    const getEditoras = async () => {
        const listaEditoras = await Conecta.get('/editora')

        setEditoras(listaEditoras.data)
    }

    const [generos, setGeneros] = useState([])
    const getGeneros = async () => {
        const listaGeneros = await Conecta.get('/genero')

        setGeneros(listaGeneros.data)
    }

    const [generosTroca, setGenerosTroca] = useState([])
    const getGenerosTroca = async () => {
        const listaGenerosTroca = await Conecta.get('/generotroca')

        setGenerosTroca(listaGenerosTroca.data)
    }

    

    useEffect(() => {
        getAutores()
        getEditoras()
        getGeneros()
        getGenerosTroca()
    }, [])
    
    const onSubmit = async (data) => {
        const novo = await Conecta.post("livro", data)

        setValue("titulo", "")
        setValue("isbn", "")
        setValue("paginas", "")
        setValue("estado", "")
        setValue("capa", "")

        alert("Livro cadastrado com sucesso")

        getAutores()
        getEditoras()
        getGeneros()
        getGenerosTroca()
        
    }

    return (
        
        <div className="row-mt-5">
            <div className="container">
                <form className='form-signin' onSubmit={handleSubmit(onSubmit)}>
                    <div className='text-center mb-4'>
                        <h1 className='mt-5 font-weight-normal'>Adicione um livro</h1>
                    </div>
                    <div className="form-row">
                        
                        <div className='form-group col-sm-12'>
                            <input
                                type='hidden'
                                id='usuario'
                                placeholder='Título'
                                required
                                value={user.dados.id}
                                {...register('usuario')}
                            />
                        </div>
                    
                        <div className='form-group col-sm-6'>
                            <input
                                type='text'
                                id='titulo'
                                className='form-control'
                                placeholder='Título'
                                required
                                autoFocus
                                {...register('titulo')}
                            />
                        </div>

                        <div className='form-group col-sm-6'>
                            <input
                                type='text'
                                id='isbn'
                                className='form-control'
                                placeholder='ISBN'
                                required
                                {...register('isbn')}
                            />
                        </div>

                        <div className='form-group col-sm-2'>
                            <input
                                type='number'
                                id='paginas'
                                className='form-control'
                                placeholder='Páginas'
                                required
                                min={1}
                                {...register('paginas')}
                            />
                        </div>

                        <div className='form-group col-sm-4'>
                            <input
                                type='text'
                                id='estado'
                                className='form-control'
                                placeholder='Estado'
                                required
                                {...register('estado')}
                            />
                        </div>

                        <div className='form-group col-sm-6'>
                            <input
                                type='text'
                                id='capa'
                                className='form-control'
                                placeholder='Link da Capa'
                                required
                                {...register('capa')}
                            />
                        </div>

                        <div className='form label-group mt-1 col-sm-6'>
                            <div className='label mb-1 font-weight-bold'>Autor:</div>
                            <select id='autor' name='autor' type='text' className='form-control' value={selectValue} required onChange={e => setSelectValue(e.target.value)} {...register('autor')}>
                                {autores.map((autor, index) => {
                                    return (
                                        <option id={autor.id} key={autor.id} value={autor.id}>{autor.nome}</option>
                                    )
                                })}
                            </select>
                        </div>

                        <div className='form label-group mt-1 col-sm-6'>
                            <div className='label mb-1 font-weight-bold'>Editora:</div>
                            <select id='editora' name='editora' type='text' className='form-control' value={selectValue} required onChange={e => setSelectValue(e.target.value)} {...register('editora')}>
                                {editoras.map((editora, index) => {
                                    return (
                                        <option id={editora.id} key={editora.id} value={editora.id}>{editora.nome}</option>
                                    )
                                })}
                            </select>
                        </div>

                        <div className='form label-group mt-3 col-sm-6'>
                        <div className='label mb-1 font-weight-bold'>Gênero:</div>
                            <select id='genero' name='genero' type='text' className='form-control' value={selectValue} required onChange={e => setSelectValue(e.target.value)} {...register('genero')}>
                                {generos.map((genero, index) => {
                                    return (
                                        <option id={genero.id} key={genero.id} value={genero.id}>{genero.nome}</option>
                                    )
                                })}
                            </select>
                        </div>

                        <div className='form label-group mt-3 col-sm-6'>
                            <div className='label mb-1 font-weight-bold'>Troca:</div>
                            <select id='generoTroca' name='generoTroca' type='text' className='form-control' value={selectValue} required onChange={e => setSelectValue(e.target.value)} {...register('generoTroca')}>
                                {generosTroca.map((generoTroca, index) => {
                                    return (
                                        <option id={generoTroca.id} key={generoTroca.id} value={generoTroca.id}>{generoTroca.nome}</option>
                                    )
                                })}
                            </select>
                        </div>
                    </div>
                    
                    <button className="btn btn-sm btn-primary btn-block mt-3" type="submit">
                        Adiciona Livro
                    </button>
                </form>
            </div>
        </div>
    )
}

export default CadLivro;