import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import Conecta from "./Conecta";
import "./tabela.css"

const Editora = () => {
    const { register, handleSubmit, setValue } = useForm();

    const [editoras, setEditoras] = useState([])

    const getEditoras = async () => {
        const lista = await Conecta.get('/editora')

        setEditoras(lista.data)
    }

    useEffect(() => {
        getEditoras()
    }, []);

    const onSubmit = async (data) => {
        const novo = await Conecta.post('editora', data)

        setValue("nome", "")

        alert("Editora cadastrada.")

        getEditoras()

    }

    return (
        <div className="row mt-5">
            <div className="container">
                <form className="formsignin" onSubmit={handleSubmit(onSubmit)}>
                    <div className="text-center">
                        <h1>Cadastro de Editora</h1>
                    </div>

                    <div className="form-row mt-3">
                        <div className="form-group col-sm-12">
                            <input
                                type="text"
                                id="nome"
                                className="form-control"
                                placeholder="Nome"
                                required
                                autoFocus
                                {...register("nome")}
                            />
                        </div>
                        
                        <button className="btn btn-lg btn-primary btn-block" type="submit">
                            Adiciona Editora
                        </button>
                    </div>
                </form>

                <div className="text-center mt-5">
                        <h1>Editoras Cadastradas</h1>
                </div>
                <div className="row mt-2">
                    <table>
                        <thead>
                        <tr>
                            <th>Nome</th>
                        </tr>
                        </thead>
                        {editoras.map((editora, index) => {
                                return (
                                    <tbody key={editora.nome}>
                                    <tr>
                                        <td>{editora.nome}</td>
                                    </tr>
                                    </tbody>
                                )
                        })}
                    </table>
                </div>
            </div>
        </div>
    )

}

export default Editora;