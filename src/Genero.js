import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import Conecta from "./Conecta";
import "./tabela.css"

const Genero = () => {
    const { register, handleSubmit, setValue } = useForm();

    const [generos, setGeneros] = useState([])

    const getGeneros = async () => {
        const lista = await Conecta.get('/genero')

        setGeneros(lista.data)
    }

    useEffect(() => {
        getGeneros()
    }, []);

    const onSubmit = async (data) => {
        const novo = await Conecta.post('genero', data)

        setValue("nome", "")

        alert("Gênero cadastrado.")

        getGeneros()

    }

    return (
        <div className="row mt-5">
            <div className="container">
                <form className="formsignin" onSubmit={handleSubmit(onSubmit)}>
                    <div className="text-center">
                        <h1>Cadastro de Gêneros</h1>
                    </div>

                    <div className="form-row mt-3">
                        <div className="form-group col-sm-12">
                            <input
                                type="text"
                                id="nome"
                                className="form-control"
                                placeholder="Nome"
                                required
                                autoFocus
                                {...register("nome")}
                            />
                        </div>
                        
                        <button className="btn btn-lg btn-primary btn-block" type="submit">
                            Adiciona Gênero
                        </button>
                    </div>
                </form>

                <div className="text-center mt-5">
                        <h1>Gêneros Cadastrados</h1>
                </div>
                <div className="row mt-2">
                    <table>
                        <thead>
                        <tr>
                            <th>Nome</th>
                        </tr>
                        </thead>
                        {generos.map((genero, index) => {
                                return (
                                    <tbody key={genero.nome}>
                                    <tr>
                                        <td>{genero.nome}</td>
                                    </tr>
                                    </tbody>
                                )
                        })}
                    </table>
                </div>
            </div>
        </div>
    )

}

export default Genero;