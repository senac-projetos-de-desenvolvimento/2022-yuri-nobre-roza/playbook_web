import React, { useContext, useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import Conecta from './Conecta'
import { UserContext } from './UserContext'
import ItemLista from './ItemLista'

const MeusLivros = () => {
    const user = useContext(UserContext)

    let { id } = useParams()

    const [livros, setLivros] = useState([])
    const getLivros = async () => {
        const listaLivros = await Conecta.get('/livro/'+id)

        setLivros(listaLivros.data)
    }
    useEffect(() => {
        getLivros()
    }, [])

    console.log(livros)
    
    return (
        <div className='container'>
            <div className='row'>
                {livros.map((livro, index) => (
                    <ItemLista
                        foto={livro.foto}
                        titulo={livro.titulo}
                        capa={livro.capa}
                        isbn={livro.isbn}
                        paginas={livro.paginas}
                        estado={livro.estado}
                        autor={livro.autor.nome}
                        editora={livro.editora.nome}
                        genero={livro.genero.nome}
                        generoTroca={livro.generoTroca.nome}
                        key={livro.id}
                    />
                ) )}
            </div>
        </div>
    )
}

export default MeusLivros