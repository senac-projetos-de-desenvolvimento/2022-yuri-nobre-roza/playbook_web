import React, { useContext } from "react";
import {Link} from "react-router-dom";
import { UserContext } from "./UserContext";
import { useNavigate } from "react-router-dom";

const Header = () => {

    const user = useContext(UserContext)
    let history = useNavigate()

    console.log(user)
    const loginLogout = () => {
      user.setDados({id: null, nome: "", admin: "", token: ""})
      history("/login")
    }

    const id = user.dados.id

    return (
        <nav className="navbar navbar-expand-sm bg-primary navbar-dark">
          <Link className="navbar-brand" to="/">
            <h3>Playbook</h3>
          </Link>
          
          <ul className="navbar-nav ml-auto">

            {user.dados.id ? <li className="nav-item">
              <Link className="nav-link" to={`/livro/${id}`} key={id}>
                Meus Livros
              </Link>
            </li> : null}
            {user.dados.id ? <li className="nav-item">
              <Link className="nav-link" to="/livro">
                Add Livro
              </Link>
            </li> : null}
            {user.dados.admin ? <li className="nav-item">
                <Link className="nav-link" to="/genero">
                  Genero
                </Link>
            </li> : null}
            {user.dados.admin ? <li className="nav-item">
                <Link className="nav-link" to="/editora">
                  Editora
                </Link>
            </li> : null}
            {user.dados.admin ? <li className="nav-item">
                <Link className="nav-link" to="/autor">
                  Autor
                </Link>
            </li> : null}
            {!user.dados.id ? <li className="nav-item">
              <Link className="nav-link" to="/cadastro">
                Cadastre-se
              </Link>
            </li> :null}

            <li className="nav-item">
              <span className="nav-link" onClick={loginLogout}>
                <i className="fas fa-user-friends mr-2"></i>
                {user.dados.nome ? user.dados.nome + " (sair)" : "Login"}
              </span>
            </li>
          </ul>
        </nav>
      );
};

export default Header;