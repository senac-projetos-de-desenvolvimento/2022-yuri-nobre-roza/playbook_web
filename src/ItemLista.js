import React from "react";
import "./ItemLista.css"

const ItemLista = (props) => {
    
    return (
        <div className="card col-sm-3 mt-2">
            <img className="card-img-top" src={props.capa} alt={props.id} />
            <div className="card-body">
                <h4 className="card-title">
                    {props.titulo}
                </h4>
                <p className="card-text">
                    ISBN: {props.isbn}
                </p>
                <p className="card-text">
                    Estado: {props.estado}
                </p>
                <p className="card-text"> 
                    Páginas: {Number(props.paginas)}
                </p> 
                <p className="card-text">
                    Autor: {props.autor}
                </p>
                <p className="card-text">
                    Editora: {props.editora}
                </p>
                <p className="card-text">
                    Gênero: {props.genero}
                </p>
                <p className="card-text">
                    Gênero Troca: {props.generoTroca}
                </p>
                <span className="float-center" onClick={props.buscaTroca}>
                    <button className="btn btn-primary mt-1" type="submit" key={props.id}>
                        Buscar Troca
                    </button>
                </span>
            </div>
        </div>
    );
};

export default ItemLista;