import React, { useContext } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router';
import Conecta from './Conecta'
import { UserContext } from './UserContext';

const UserLogin = () => {

    const { register, handleSubmit } = useForm();

    let history = useNavigate()
    const user = useContext(UserContext)

    const onSubmit = async (data) => {
        const login = await Conecta.post("login", data)

        if (login.data.validaEmail) {
            user.setDados({
                id: login.data.validaEmail.id,
                nome: login.data.validaEmail.nome,
                token: login.data.token,
                admin: login.data.validaEmail.admin
            })
            history('/livro')
        } else {
            alert("Login inválido")
        }
        console.log(login)
    }

    return(
        <div className="row mt-5">
            <div className="container">
                <form className="formsignin" onSubmit={handleSubmit(onSubmit)}>
                    <div className="text-center">
                        <h1>Login</h1>
                    </div>

                    <div className="form-row mt-3">
                        <div className="form-group col-sm-12">
                            <input
                                type="text"
                                id="email"
                                className="form-control"
                                placeholder="E-mail"
                                required
                                autoFocus
                                {...register("email")}
                            />
                        </div>

                        <div className="form-group- col-sm-12">
                            <input
                                type="password"
                                id="senha"
                                className="form-control"
                                placeholder="Senha"
                                required
                                {...register("senha")}
                            />
                        </div>
                        
                        <button className="btn btn-lg btn-primary btn-block mt-3" type="submit">
                            Entrar
                        </button>
                    </div>
                </form>
            </div>
        </div>   
    )

}

export default UserLogin;