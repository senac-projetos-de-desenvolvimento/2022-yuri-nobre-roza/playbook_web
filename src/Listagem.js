import React, {useState, useEffect} from "react";
import Conecta from "./Conecta";
import ItemLista from "./ItemLista";

const Listagem = () => {
    const [books, setBooks] = useState([]);
    
    const getBooks = async () => {
        const lista = await Conecta.get("/book");

        setBooks(lista.data)
    }
    
    useEffect(() => {
        getBooks();
    }, []);

    console.log(books);

    return (
        <div className="container">
            <div className="row">
                {books.map((book, index) => (
                    <ItemLista
                        nome={book.nome}
                        id={book.id}
                        isbn={book.isbn}
                        autor={book.autor}
                        editora={book.editora}
                        genero={book.genero}
                        genetroTroca={book.genetroTroca}
                        key={book.id}
                    />
                ))}
            </div>
        </div>
    );
};

export default Listagem;