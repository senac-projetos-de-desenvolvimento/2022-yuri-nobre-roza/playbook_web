import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import Conecta from "./Conecta";
import "./tabela.css"

const Autor = () => {
    const { register, handleSubmit, setValue } = useForm();

    const [autores, setAutores] = useState([])

    const getAutores = async () => {
        const lista = await Conecta.get('/autor')

        setAutores(lista.data)
    }

    useEffect(() => {
        getAutores()
    }, []);

    const onSubmit = async (data) => {
        const novo = await Conecta.post('autor', data)

        setValue("nome", "")
        setValue("nacionalidade", "")

        alert("Autor cadastrado.")

        getAutores()

    }

    return (
        <div className="row mt-5">
            <div className="container">
                <form className="formsignin" onSubmit={handleSubmit(onSubmit)}>
                    <div className="text-center">
                        <h1>Cadastro de Autor</h1>
                    </div>

                    <div className="form-row mt-3">
                        <div className="form-group col-sm-6">
                            <input
                                type="text"
                                id="nome"
                                className="form-control"
                                placeholder="Nome"
                                required
                                autoFocus
                                {...register("nome")}
                            />
                        </div>
                        <div className="form-group col-sm-6">
                            <input
                                type="text"
                                id="nacionalidade"
                                className="form-control"
                                placeholder="Nacionalidade"
                                required
                                {...register("nacionalidade")}
                            />
                        </div>

                        <button className="btn btn-lg btn-primary btn-block" type="submit">
                            Adiciona Autor
                        </button>
                    </div>
                </form>

                <div className="text-center mt-5">
                        <h1>Autores Cadastrados</h1>
                </div>
                <div className="row mt-2">
                    <table>
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Nacionalidade</th>
                        </tr>
                        </thead>
                        {autores.map((autor, index) => {
                                return (
                                    <tbody key={autor.nome}>
                                    <tr>
                                        <td>{autor.nome}</td>
                                        <td>{autor.nacionalidade}</td>
                                    </tr>
                                    </tbody>
                                )
                        })}
                    </table>
                </div>
            </div>
        </div>
    )

}

export default Autor;