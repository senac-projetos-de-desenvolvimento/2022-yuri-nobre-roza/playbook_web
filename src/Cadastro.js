import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import Conecta from './Conecta';

const Cadastro = () => {

    const { register, handleSubmit, setValue } = useForm()

    const [selectValue, setSelectValue] = useState()

    const [cidades, setCidades] = useState([])    
    const getCidades = async () => {
        const lista = await Conecta.get('/cidade')

        setCidades(lista.data)
    }

    useEffect(() => {
        getCidades()
    }, []);


    const onSubmit = async (data) => {
        const novo = await Conecta.post("usuario", data)

        setValue("nome", "")
        setValue("sobrenome", "")
        setValue("email", "")
        setValue("senha", "")
        setValue("endereco", "")
        setValue("cep", "")

        alert("Seja bem vindo ao playbook")

        getCidades()
    }

    return (
        <div className="row mt-5">
        <div className="container">
          <form className="form-signin" onSubmit={handleSubmit(onSubmit)}>
            <div className="text-center mb-4">
              <h1 className="mb-3 font-weight-normal">Cadastre-se</h1>
            </div>
            <div className="form-row">

                <div className="form-group col-sm-6">
                <input
                    type="text"
                    id="nome"
                    className="form-control"
                    placeholder="Nome"
                    required
                    autoFocus
                    {...register("nome")}
                />
                </div>
    
                <div className="form-group col-sm-6">
                <input
                    type="text"
                    id="sobrenome"
                    className="form-control"
                    placeholder="Sobrenome"
                    required
                    {...register("sobrenome")}
                />
                </div>

                <div className="form-label-group col-sm-6">
                <input
                    type="email"
                    id="email"
                    className="form-control"
                    placeholder="E-mail"
                    required
                    {...register("email")}
                />
                </div>

                <div className="form-label-group col-sm-6">
                <input
                    type="password"
                    id="senha"
                    className="form-control"
                    placeholder="Senha"
                    required
                    {...register("senha")}
                />
                </div>

                <div className="form-label-group mt-2 col-sm-9">
                <input
                    type="text"
                    id="endereco"
                    className="form-control"
                    placeholder="Endereço"
                    required
                    {...register("endereco")}
                />
                </div>

                <div className="form-label-group mt-2 col-sm-3">
                <input
                    type="text"
                    id="cep"
                    className="form-control"
                    placeholder="CEP"
                    required
                    {...register("cep")}
                />
                </div>

                <div className="form-label-group mt-2 col-sm-6">
                    <select id="cidade" name="cidade" type="text" className="form-control" value={selectValue} required onChange={e => setSelectValue(e.target.value)} {...register("cidade")}>
                        {cidades.map((cidade, index) => {
                            return (
                                <option id={cidade.id} key={cidade.id} value={cidade.id}>{cidade.nome}</option>
                            )
                        })}
                    </select>
                </div>
            </div>
  
            <button className="btn btn-sm btn-primary btn-block mt-5" type="submit">
              Cadastre-se
            </button>
          </form>
        </div>
      </div>
    )
}

export default Cadastro;