import React, { useState } from "react";
import './App.css';
import Header from './Header';
import { BrowserRouter as Router, Routes, Route} from "react-router-dom";
import Listagem from "./Listagem";
import CadLivro from "./CadLivro";
import Cadastro from "./Cadastro";
import Autor from "./Autor";
import Editora from "./Editora";
import Genero from "./Genero";
import Login from "./Login";
import { UserContext } from "./UserContext";
import MeusLivros from "./MeusLivros";

function App() {

  const [dados, setDados] = useState({})


  return (
    <UserContext.Provider value={{dados, setDados}}>
    <Router>
      <Header />
      <Routes>
        <Route path="/livro/:id" element={<MeusLivros />} />
        <Route path="/livro" element={<CadLivro />} /> 
        <Route path="/listagem" element={<Listagem />} />
        <Route path="/cadastro" element={<Cadastro />} />
        <Route path="/autor" element={<Autor />} />
        <Route path="/editora" element={<Editora />} />
        <Route path="/genero" element={<Genero />} />
        <Route path="/login" element={<Login />} />
      </Routes>
    </Router>
    </UserContext.Provider>
  );
}

export default App;
